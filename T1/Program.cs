﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T1
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int colum = 5;
            int rows = 4;

            int[,] mas = new int[rows,colum];

            for (int i=0; i<rows;i++)
            {
                for(int j=0;j<colum;j++)
                {
                    mas[i,j] = rnd.Next(1, 10 + 1);
                }
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colum; j++)
                {
                    Console.Write("{0,-5}",mas[i, j]);
                }
                Console.WriteLine();
            }

            Console.WriteLine("реверс столбцов:");

            int temp;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colum/2; j++)
                {
                    temp = mas[i, colum - j - 1];
                    mas[i, colum - j-1] = mas[i, j];
                    mas[i, j] = temp;            
                }
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colum; j++)
                {
                    Console.Write("{0,-5}", mas[i, j]);
                }
                Console.WriteLine();
            }

            Console.WriteLine("реверс строк:");

            for (int i = 0; i < rows/2; i++)
            {
               
                for (int j = 0; j < colum; j++)
                {
                    temp = mas[rows - i - 1,j];
                    mas[rows - i - 1,j] = mas[i, j];
                    mas[i, j] = temp;
                }
            }

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colum; j++)
                {
                    Console.Write("{0,-5}", mas[i, j]);
                }
                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
