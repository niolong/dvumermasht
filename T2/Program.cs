﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();

            int rows = 18;
            int colum = 36;
            int[,] mas=new int[rows,colum];
            int countFreePlace=0;

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < colum; j++)
                {
                    mas[i, j] = 0;
                }
            }

            int place;
            int vagon;
            char ans = 'n';

            Console.WriteLine("Если хотите рандомно заполнить места в вагоне введите: r, для отказа n");
            ans = char.Parse(Console.ReadLine());

            if (ans == 'r')
            {
                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < colum; j++)
                    {
                        mas[i, j] = rnd.Next(0, 1 + 1);
                    }
                }

                for (int i = 0; i < rows; i++)
                {
                    countFreePlace = 0;
                    for (int j = 0; j < colum; j++)
                    {
                        if (mas[i, j] == 0)
                        {
                            countFreePlace++;
                        }
                    }
                    Console.WriteLine($"Свободных мест в вагоне №{i} = {countFreePlace-1}");
                }

                Console.WriteLine("Хотите выбрать места в заполненом вагоне?(y/n)");
                ans = char.Parse(Console.ReadLine());
            }

            if (ans=='y'|| ans=='n')
            {
                do
                {
                    Console.WriteLine("Введите вагон(от 0 до 17):");
                    vagon = int.Parse(Console.ReadLine());

                    for (int i = vagon; i <= vagon; i++)
                    {
                        countFreePlace = 0;
                        for (int j = 0; j < colum; j++)
                        {
                            if (mas[i, j] == 0)
                            {
                                countFreePlace++;
                            }
                        }
                        Console.WriteLine($"Свободных мест в вагоне №{i} = {countFreePlace - 1}");
                    }

                    Console.WriteLine("Введите место:");
                    place = int.Parse(Console.ReadLine());

                    if (mas[vagon, place] == 0)
                    {
                        mas[vagon, place] = 1;
                    }
                    else
                    {
                        while (mas[vagon, place] == 1)
                        {
                            Console.WriteLine("Место занято, выберите другое:");
                            place = int.Parse(Console.ReadLine());
                        }
                    }

                    Console.WriteLine("Хотите выбрать еще(y/n)");
                    ans = char.Parse(Console.ReadLine());

                } while (ans == 'y');
            }
            Console.ReadKey();
        }
    }
}
