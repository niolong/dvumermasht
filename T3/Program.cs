﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] mas1;
            int[] mas2;
            int leghtM1, leghtM2;
            int sum1=0, sum2=0;

            Console.WriteLine("введите длину 1го числа");
            leghtM1 = int.Parse(Console.ReadLine());

            //1е число

            mas1 = new int[leghtM1];

            Console.WriteLine("Введите 1е число по цифре");

            for (int i=0;i<leghtM1;i++)
            {
                mas1[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < leghtM1; i++)
            {
                sum1 = sum1 + mas1[i];
            }

            //2е число

            Console.WriteLine("введите длину 2го числа");
            leghtM2 = int.Parse(Console.ReadLine());

            mas2 = new int[leghtM2];

            Console.WriteLine("Введите 2е число по цифре");

            for (int i = 0; i < leghtM2; i++)
            {
                mas2[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 0; i < leghtM2; i++)
            {
                sum2 = sum2 + mas2[i];
            }

            if(sum1>sum2)
            {
                Console.WriteLine("1");
            }
            else if(sum1<sum2)
            {
                Console.WriteLine("-1");
            }
            else if(sum1==sum2)
            {
                Console.WriteLine("0");
            }

            Console.ReadKey();
        }
    }
}
